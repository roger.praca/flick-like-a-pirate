# Flicklikeapirateng

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Running Project

Run `npm run start` to start server. Navigate to `http://localhost:8181/`.  

## Running unit tests

Run `npm run test-express` to execute unit test via [Mocha](https://mochajs.org/).

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

