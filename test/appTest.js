var assert = require('chai').assert;
var request = require('supertest');
var serverLoaders = require('../app');
const isNil = (val) => (val===null || val===undefined)
describe('loading express', function () {
	var app;
	var flickrUri = '/service/rest';

	beforeEach(function () {
		while(isNil(app)) {
			app = serverLoaders();
		}
	});
	
	it('responds to /', function testRoot(done) {
		request(app).get('/').expect(200, done);
	});
	// flickr API we are going to use
	it('flickr.photos.getRecent', function testRoot(done) {
		request(app).post(flickrUri)
		.send({method:"flickr.photos.getRecent"})
		.expect('Content-Type', /json/)
		.expect(function(resp){
//			console.log(resp.body.photos);
			assert(typeof resp.body.photos, 'object');
		}).expect(200, done);
	});
	it('photos_public.gne', function(done){
		request(app).post(flickrUri)
		.send({method:"photos_public.gne"})
		.expect('Content-Type', /json/)
		.expect(function(resp){
			console.log(resp.body);
			assert(typeof resp.body.photos, 'object');
		}).expect(200, done);
	});
})
