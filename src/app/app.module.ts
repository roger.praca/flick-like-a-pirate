import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MynavComponent } from './mynav/mynav.component';
import { FlickrFeedViewerComponent } from './flickr-feed-viewer/flickr-feed-viewer.component';
import {FlickrService} from './flickr.service';
import { FlickrFeedPhotoTileComponent } from './flickr-feed-photo-tile/flickr-feed-photo-tile.component';

@NgModule({
  declarations: [
    AppComponent,
    MynavComponent,
    FlickrFeedViewerComponent,
    FlickrFeedPhotoTileComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    FlickrService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
