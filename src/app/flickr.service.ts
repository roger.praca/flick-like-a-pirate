import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FlickrService {
  furl = 'service/rest';
  defExtras = 'media,date_upload,tags,owner_name,url_sq,url_t,url_s,url_q,url_m,url_n,url_z,url_c,url_l,url_o';
  txtExtras: string;
  constructor(private http: HttpClient) { }
  getUrl() {
    return window.location.href + this.furl;
  }
  public setExtras(extrasStr: string) {
    this.txtExtras = extrasStr;
  }
  public getExtras() {
    return (this.txtExtras && this.txtExtras.length > 0) ? this.txtExtras : this.defExtras;
  }
  public searchPhotos(txttags: string) {
    const pBody = {
      method: 'flickr.photos.search',
      tags: txttags.replace(/\ /g, ','),
      extras: this.getExtras()
    };

    return this.http.post<any[]>(this.getUrl(), pBody);
  }
}
