import { Component, OnInit, Input } from '@angular/core';
import {FlckFeedPhoto} from '../model/flck-feed-photo';

@Component({
  selector: 'app-flickr-feed-photo-tile',
  templateUrl: './flickr-feed-photo-tile.component.html',
  styleUrls: ['./flickr-feed-photo-tile.component.scss']
})
export class FlickrFeedPhotoTileComponent implements OnInit {
  @Input()
  flckphoto: FlckFeedPhoto;
  constructor() { }
  ngOnInit() { }
  loadEmpty() {
    this.flckphoto = {
      title: '',
      link: '',
      media: {m: ''},
      date_taken: '',
      description: '',
      published: '',
      author: '',
      author_id: '',
      tags: ''
    };
  }
}
