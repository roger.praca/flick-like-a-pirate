import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlickrFeedPhotoTileComponent } from './flickr-feed-photo-tile.component';

describe('FlickrFeedPhotoTileComponent', () => {
  let component: FlickrFeedPhotoTileComponent;
  let fixture: ComponentFixture<FlickrFeedPhotoTileComponent>;

  const flckPhotoTest = {
    title: 'নেতাজি ছিলেন শক্তিরই প্রতিভূ',
    link: 'https://www.flickr.com/photos/155354419@N08/46121812714/',
    media: {m: 'https://farm8.staticflickr.com/7842/46121812714_9df77e4ab4_m.jpg'},
    date_taken: '2019-01-22T22:36:35-08:00',
    description: 'gaita',
    published: '2019-01-23T06:36:35Z',
    author: 'nobody@flickr.com ("rsbd2500")',
    author_id: '155354419@N08',
    tags: ''
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlickrFeedPhotoTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlickrFeedPhotoTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
