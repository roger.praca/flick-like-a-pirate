import { Component, Input, OnInit, OnChanges, ElementRef, AfterViewInit, Inject, Renderer2, ChangeDetectorRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {FlickrService} from '../flickr.service';
import {FlckFeedPhoto} from '../model/flck-feed-photo';


// required to use JSON-P approach
declare global {
  interface Window { jsonFlickrFeed: any; }
}
@Component({
  selector: 'app-flickr-feed-viewer',
  templateUrl: './flickr-feed-viewer.component.html',
  styleUrls: ['./flickr-feed-viewer.component.scss']
})
export class FlickrFeedViewerComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() taglist: string;
  private _pdata: FlckFeedPhoto[];
  scp: any;
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private flickr: FlickrService,
    @Inject(DOCUMENT) private document,
    private elementRef: ElementRef,
    private renderer: Renderer2) {
  }
  protected loadData(data: FlckFeedPhoto[]) {
    this.rmData();
    while ( data.length > 0) {
      const p = data.pop();
      this._pdata.unshift(p);
    }
    this.changeDetectorRef.detectChanges();
  }
  protected rmData() {
    while (this._pdata.length > 0) {
      this._pdata.pop();
    }
    this.changeDetectorRef.detectChanges();
  }
  public hasData() {
    return this._pdata.length > 0;
  }
  public addScript() {
    this.renderer.appendChild(this.elementRef.nativeElement, this.scp);
  }
  public rmScript() {
    this.renderer.removeChild(this.elementRef.nativeElement, this.scp);
  }
  set pdata(newdata: FlckFeedPhoto[]) {
    this._pdata = newdata;
  }
  get pdata() {
    return this._pdata;
  }
  processPData(data) {
    const convertToflckFeedPhoto = (dt: any) => {
      return  {
        title: dt.title,
        link: '',
        media: {m: dt.url_m},
        date_taken: '',
        description: '',
        published: dt.dateupload,
        author: dt.ownername,
        author_id: dt.owner,
        tags: dt.tags
      };
    };
    if (data && data.photos && data.photos.photo) {
      this.loadData(data.photos.photo.map(convertToflckFeedPhoto));
    }
  }
  ngOnInit() {
    // JSON-P script element
    this.scp = this.document.createElement('script');
    this.scp.src = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json';
    this.scp.type = 'text/javascript';
    const self = this;
    this._pdata = [];
    // JSON-P callback
    window.jsonFlickrFeed = (data) => {
      self.loadData(data.items);
    };
  }
  ngAfterViewInit() {
    this.addScript();
  }

  ngOnChanges() {
    if (this.taglist.length > 0) {
      const req = this.flickr.searchPhotos(this.taglist);
      const self = this;
      req.subscribe(data => self.processPData(data));
    }
  }
}
