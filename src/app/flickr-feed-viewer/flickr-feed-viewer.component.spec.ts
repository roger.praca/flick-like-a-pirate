import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { FlickrFeedViewerComponent } from './flickr-feed-viewer.component';
import { FlickrFeedPhotoTileComponent } from '../flickr-feed-photo-tile/flickr-feed-photo-tile.component';


describe('FlickrFeedViewerComponent', () => {
  let component: FlickrFeedViewerComponent;
  let fixture: ComponentFixture<FlickrFeedViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [
        FlickrFeedViewerComponent,
        FlickrFeedPhotoTileComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlickrFeedViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
