import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MynavComponent } from './mynav/mynav.component';
import { FlickrFeedViewerComponent } from './flickr-feed-viewer/flickr-feed-viewer.component';
import { FlickrFeedPhotoTileComponent } from './flickr-feed-photo-tile/flickr-feed-photo-tile.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule
      ],
      declarations: [
        AppComponent,
        MynavComponent,
        FlickrFeedViewerComponent,
        FlickrFeedPhotoTileComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    // async(inject([HttpTestingController],
    //   (httpClient: HttpTestingController) => {
    // }));
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Flicking Like a Pirate'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Flicking Like a Pirate');
  });

  it('should render title in a app-mynav tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-mynav').textContent).toContain('Flicking Like a Pirate');
  });

  it('should update ptags when onSearchReq is called', () => {
    const testTags = 'banana split';
    const fixture = TestBed.createComponent(AppComponent);
    fixture.componentInstance.onSearchReq(testTags);
    expect(fixture.componentInstance.ptags).toEqual(testTags);
  });

});
