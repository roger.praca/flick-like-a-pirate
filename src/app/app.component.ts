import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Flicking Like a Pirate';
  ptags = '';
  public onSearchReq(tags: string) {
    this.ptags = tags;
  }
}
