import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mynav',
  templateUrl: './mynav.component.html',
  styleUrls: ['./mynav.component.scss']
})
export class MynavComponent implements OnInit {
  @Input() title: string;
  @Output() toSearch: EventEmitter<any> = new EventEmitter();

  qform = { searchTags: '' };

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    const tags = `${this.qform.searchTags}`;
    this.qform.searchTags = '';
    this.toSearch.emit(tags);
  }
}
